{-# LANGUAGE DeriveFunctor #-}
-- включение возможности вывода (deriving) функтора

module Types.Interpreter where

-- модуль из free
import Control.Monad.Free

import Types

--------------------------------------------------------------------------
-- Далее: "наивный" подход, список команд вручную
-- Не можем использовать do-нотацию и другие возможности Haskell
--------------------------------------------------------------------------
-- data Cmd
--   = PrintRemaining
--   | PrintIsAllowed Int
--   | Take
--   deriving (Eq,Show,Read)
--
-- type Program = [Cmd]
--
-- interpret1 :: Program -> GameState -> IO ()
-- interpret1 [] _ = pure ()
-- interpret1 (PrintRemaining : cmds) gs@GameState{gsRemaining = n}
--   = print n >> interpret1 cmds gs
-- interpret1 (PrintIsAllowed m : cmds) gs@GameState{gsAllowed = k}
--   = print (m <= k) >> interpret1 cmds gs
-- interpret1 (Take : cmds) gs@GameState{gsRemaining = n} = do
--   m <- readLn
--   interpret1 cmds (gs{gsRemaining = n-m})
--
-- program0 = [PrintRemaining, PrintIsAllowed 3, Take, PrintRemaining]

--------------------------------------------------------------------------------
-- Подход со свободной монадой
--------------------------------------------------------------------------------

-- Объявляем наши команды
-- next - дальнейшее вычисление
data CmdF next
  = PrintRemaining next
  | PrintIsAllowed Int next
  | Take (Int -> next)
  deriving (Functor)

type Program = Free CmdF

-- Можем использовать deriving (Functor) вместо написания вручную

-- instance Functor CmdF where
--   fmap f (PrintRemaining next) = PrintRemaining (f next)
--   fmap f (PrintIsAllowed m next) = PrintIsAllowed m (f next)
--   fmap f (Take next) = Take (f . next)

-- Свободная монада, по сути, будет развернута в следующий тип (список команд):

-- data FreeCmdF a
--   = Pure a
--   | FreePR (FreeCmdF a)
--   | FreePIA Int (FreeCmdF a)
--   | FreeT (Int -> FreeCmdF a)

-- Интерпретатор
interpret2 :: GameState -> Program a -> IO a
interpret2 _ (Pure a) = pure a
interpret2 gs (Free f) = case f of
  PrintRemaining next -> print (gsRemaining gs) >> interpret2 gs next
  PrintIsAllowed m next -> print (m <= gsAllowed gs) >> interpret2 gs next
  Take next -> do
    m <- readLn
    interpret2 gs{gsRemaining = gsRemaining gs - m} (next m)

-- Базовые команды
printRemaining :: Program ()
printRemaining = liftF $ PrintRemaining ()

printIsAllowed :: Int -> Program ()
printIsAllowed k = liftF $ PrintIsAllowed k ()

takeC :: Program Int
takeC = liftF $ Take id

-- Можем вводить дополнительные команды, не меняя базовых типов и
-- интерпретаторов
takeAndPrint :: Program ()
takeAndPrint = do
  m <- takeC
  printIsAllowed m
  printRemaining

-- Если экспортировать из модуля только команды printRemaining, printIsAllowed,
-- takeC, takeAndPrint - пользователю будет казаться, что он работает с
-- подмножеством Haskell (см. примеры программ). Кроме того, он получит
-- возможность композирования программ

--------------------------------------------------------------------------------
-- Примеры программ
--------------------------------------------------------------------------------

program1, program2 :: Program ()

program1 = do
  printRemaining
  m <- takeC
  printIsAllowed m
  printRemaining

program2 = do
  takeAndPrint
  takeAndPrint

--------------------------------------------------------------------------------
-- Запуск программ
--------------------------------------------------------------------------------

example1 = interpret2 (GameState 10 3 True) program2

