module Lib.DrawTest where

import Graphics.Gloss
import Graphics.Gloss.Interface.IO.Interact

type World = [(Float, Float)]

initialWorld :: World
initialWorld = [(50,100)]

draw :: World -> Picture
draw = Pictures . map drawCircle
  where drawCircle (x,y) = Translate x y $ Circle 50

handleEvent :: Event -> World -> World
handleEvent (EventKey (MouseButton LeftButton) Down _ (x,y)) w
  = (x,y) : w
handleEvent _ w = w

handleTime :: Float -> World -> World
handleTime _ w = w

appDrawTest :: IO ()
appDrawTest = play FullScreen white 100 initialWorld draw handleEvent handleTime

