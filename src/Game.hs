module Game where

import Types

unless :: e -> Bool -> Either e ()
unless e b = if b then Right () else Left e

makeMove :: GameState
         -> Int
         -> Either MoveError GameState
makeMove gs k = do
  GameIsOver `unless` (gsRemaining gs > 0)
  TakesLessThanOne `unless` (k > 0)
  TakesMoreThanAllowed `unless` (k <= gsAllowed gs)
  pure $ gs { gsRemaining = gsRemaining gs - k
            , gsFirst = not $ gsFirst gs}

firstWon :: GameState -> Maybe Bool
firstWon GameState { gsRemaining = 0, gsFirst = f } = Just $ not f
firstWon _ = Nothing

