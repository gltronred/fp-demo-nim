module Lib (appDrawTest, appMain) where

import Data.Char
import Graphics.Gloss
import Graphics.Gloss.Interface.IO.Interact
import System.Environment
import System.Random

import Game
import Lib.DrawTest (appDrawTest)
import Types

data World
  = GameOver { playerWon :: Bool }
  | InputError { gameState :: GameState, gen :: StdGen, time :: Float }
  | MoveInProgress { gameState :: GameState, gen :: StdGen, move :: Int, time :: Float}
  | AIMove { gameState :: GameState, gen :: StdGen, time :: Float }
  | PlayerMove { gameState :: GameState, gen :: StdGen }
  deriving (Show,Read)

draw :: World -> Picture
draw (GameOver True)  = translate (-10) 50 $ color green $ text "You won!"
draw (GameOver False) = translate (-10) 50 $ color red $ text "You lose!"
draw (InputError gs _ _) = drawGameState gs 0 <>
                         translate 50 50 (color red $ rectangleSolid 100 50)
draw (MoveInProgress gs _ m _) = drawGameState gs m
draw (AIMove gs _ _) = drawGameState gs 0 <> translate (-100) (-100) (text "Thinking...")
draw (PlayerMove gs _) = drawGameState gs 0 <> translate (-100) (-100) (text "Your move...")

drawGameState :: GameState -> Int -> Picture
drawGameState GameState { gsRemaining = n, gsAllowed = k, gsFirst = f } move
  = pictures $ map (drawOne . toRectCoords) [1..n]
  where
    cols = 13
    toRectCoords m = ( fromIntegral $ (m-1)`div`cols
                     , fromIntegral $ (m-1)`mod`cols
                     , m)
    wx = 50.0
    wy = 50.0
    lx = 400
    ly = 100
    drawOne (i,j,m) = translate (wx*j-lx) (wy*i-ly) $
                      color (if m+k>n
                             then if m+move > n
                                  then yellow
                                  else green
                             else red) $
                      circleSolid 20

onEvent :: Event -> World -> World
onEvent (EventKey (Char c) Up _ _) (PlayerMove gs g)
  | '1' <= c && c <= '9' = let
      move = ord c - ord '0'
      in case makeMove gs move of
           Left err -> InputError gs g 0
           Right _ -> MoveInProgress gs g move 0
  | otherwise = PlayerMove gs g
onEvent _ gs = gs

onTime :: Float -> World -> World
onTime dt world = case world of
  InputError gs g t | t + dt > 0.5 -> PlayerMove gs g
                    | otherwise -> InputError gs g $ t + dt
  MoveInProgress gs g m t | t + dt > 1 -> whoMoves g gs m
                          | otherwise -> MoveInProgress gs g m $ t+dt
  AIMove gs g t | t+dt > 0.7 -> let
                    (m, g') = aiMove g gs
                    in MoveInProgress gs g' m 0
                | otherwise -> AIMove gs g $ t+dt
  _ -> world

whoMoves :: StdGen -> GameState -> Int -> World
whoMoves g gs0 m = case firstWon gs of
  Just winner -> GameOver winner
  Nothing -> if gsFirst gs
             then PlayerMove gs g
             else AIMove gs g 0
  where Right gs = makeMove gs0 m

aiMove :: StdGen -> GameState -> (Int, StdGen)
aiMove gen GameState{ gsAllowed = k } = randomR (1,k) gen

appMain :: IO ()
appMain = do
  args <- getArgs
  case args of
    [allowedStr] -> do
      remaining <- randomRIO (10,20)
      g <- getStdGen
      let allowed = read allowedStr
          initial = PlayerMove { gameState = GameState { gsRemaining = remaining
                                                       , gsAllowed = allowed
                                                       , gsFirst = True}
                               , gen = g }
      play FullScreen white 100 initial draw onEvent onTime
    _ -> putStrLn "Usage: nim <allowed>"

