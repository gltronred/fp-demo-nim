module Types where

data MoveError
  = TakesMoreThanAllowed
  | TakesLessThanOne
  | GameIsOver
  deriving (Eq,Show,Read)

data GameState = GameState
  { gsRemaining :: Int
  , gsAllowed :: Int
  , gsFirst :: Bool
  } deriving (Eq,Show,Read)

newtype State = State { getGames :: [GameState] }
  deriving (Eq,Show,Read)



