{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Main where

import qualified Test.SmallCheck as SC
import qualified Test.SmallCheck.Series as SC
import           Test.Tasty
import           Test.Tasty.HUnit
import           Test.Tasty.QuickCheck as QC
import           Test.Tasty.SmallCheck as SC

import           Game
import           Types

main = defaultMain tests

tests :: TestTree
tests = testGroup "Tests"
  [ makeMoveTests
  , firstWonTests
  ]

makeMoveTests = testGroup "makeMove"
  [ testCase "makeMove (9,3,True) 3 == (6,3,False)" $
    makeMove (GameState 9 3 True) 3 @?= Right (GameState 6 3 False)
  , testCase "makeMove (0,3,True) 1 == GameIsOver" $
    makeMove (GameState 0 3 True) 1 @?= Left GameIsOver
  , testCase "makeMove (3,3,True) (-1) == TakesLessThanOne" $
    makeMove (GameState 3 3 True) (-1) @?= Left TakesLessThanOne
  , testCase "makeMove (5,3,True) 5 == TakesMoreThanAllowed" $
    makeMove (GameState 5 3 True) 5 @?= Left TakesMoreThanAllowed
  , QC.testProperty "makeMove decreases remaining" $
    \gs k -> gsRemaining gs > 0  QC.==>
             either (const 0) gsRemaining (makeMove gs k) < gsRemaining gs
  , SC.testProperty "makeMove changes side" $
    \gs k -> k > 0 && k <= gsAllowed gs && gsRemaining gs > 0  SC.==>
             (gsFirst <$> makeMove gs k) == Right (not $ gsFirst gs)
  ]

instance Arbitrary GameState where
  arbitrary = GameState
              <$> (getNonNegative <$> arbitrary)
              <*> (getNonNegative <$> arbitrary)
              <*> arbitrary

instance Monad m => SC.Serial m GameState where
  series = GameState
              <$> (SC.getNonNegative <$> SC.series)
              <*> (SC.getNonNegative <$> SC.series)
              <*> SC.series

firstWonTests = testGroup "firstWon"
  [
  ]

